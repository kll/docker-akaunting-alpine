FROM gslime/php-alpine

LABEL Maintainer Kevin Lewis <docker@kevin.oakaged.io>

EXPOSE 80/tcp 9000/tcp

# Add Build Dependencies
RUN apk add --no-cache --virtual .build-deps  \
    zlib-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libxml2-dev \
    libzip-dev \
    bzip2-dev

# Add Production Dependencies
RUN apk add --update --no-cache \
    bash \
    php7-fpm \
    jpegoptim \
    pngquant \
    optipng \
    supervisor \
    vim \
    icu-dev \
    freetype-dev \
    nginx \
    postgresql-dev \
    mysql-client

# Configure & Install Extension
RUN docker-php-ext-configure \
    opcache --enable-opcache &&\
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ &&\
    docker-php-ext-install \
    opcache \
    mysqli \
    pgsql \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    sockets \
    json \
    intl \
    gd \
    xml \
    zip \
    bz2 \
    pcntl \
    bcmath

# Add Composer
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="./vendor/bin:$PATH"

COPY opcache.ini $PHP_INI_DIR/conf.d/
COPY php.ini $PHP_INI_DIR/conf.d/

# Setup Crond and Supervisor by default
RUN echo '*  *  *  *  * /usr/local/bin/php  /var/www/artisan schedule:run >> /dev/null 2>&1' > /etc/crontabs/root && mkdir /etc/supervisor.d
ADD master.ini /etc/supervisor.d/

# Remove Build Dependencies
RUN apk del -f .build-deps

# Configure php-fpm
RUN sed -i 's/^listen = .*/listen = 0.0.0.0:9000/' /etc/php7/php-fpm.d/www.conf \
 && sed -i 's/^;env\[PATH\]/env\[PATH\]/' /etc/php7/php-fpm.d/www.conf

ENV AKAUNTING_VERSION=1.3.17 \
    AKAUNTING_USER=www-data \
    AKAUNTING_INSTALL_DIR=/var/www/akaunting \
    AKAUNTING_DATA_DIR=/var/lib/akaunting \
    AKAUNTING_CACHE_DIR=/etc/docker-akaunting

ENV AKAUNTING_BUILD_DIR=${AKAUNTING_CACHE_DIR}/build \
    AKAUNTING_RUNTIME_DIR=${AKAUNTING_CACHE_DIR}/runtime

COPY assets/build/ ${AKAUNTING_BUILD_DIR}/

RUN sh ${AKAUNTING_BUILD_DIR}/install.sh

COPY assets/runtime/ ${AKAUNTING_RUNTIME_DIR}/

COPY assets/tools/ /usr/bin/

COPY akaunting-entrypoint.sh /usr/local/bin/akaunting-entrypoint

RUN chmod 755 /usr/local/bin/akaunting-entrypoint

WORKDIR ${AKAUNTING_INSTALL_DIR}

CMD ["/usr/bin/supervisord"]
