# Akaunting on Alpine Linux

My take on an Akaunting setup using Alpine linux.

## Sources

I heavily used these repositories when creating my own setup.

* [sameersbn/docker-akaunting](https://github.com/sameersbn/docker-akaunting)
* [kamerk22/laravel-alpine](https://github.com/kamerk22/laravel-alpine)

## License

The code in this repository, unless otherwise noted, is MIT licensed. See the [LICENSE](LICENSE) file in this repository.